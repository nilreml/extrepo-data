---
stages:
- docker
- test
- publish

.build-docker: &build-docker
  stage: docker
  image: docker:latest
  services:
  - docker:dind
  script:
  - cd docker
  - docker build -t $CI_REGISTRY_IMAGE/tor:$DOCKER_TAG .
  after_script:
  - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  - docker push $CI_REGISTRY_IMAGE/tor:$DOCKER_TAG

build-docker-master:
  variables:
    DOCKER_TAG: latest
  rules:
  - if: $CI_COMMIT_REF_NAME == "master"
    changes:
    - docker/**/*
  <<: *build-docker

build-docker-branch:
  variables:
    DOCKER_TAG: $CI_COMMIT_REF_NAME
  rules:
  - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    changes:
    - docker/**/*
    - .gitlab-ci.yml
  <<: *build-docker

pages:
  stage: publish
  script:
  - perl tools/check-repos repos/debian/*yaml
  - perl tools/process
  - for i in public/*/*/index.yaml; do gpg -a -b --local-user $GPG_SIGNING_KEY $i; done
  artifacts:
    paths:
    - public
  tags:
  - extrepo
  only:
  - master
  except:
  - schedules

check-repos:
  stage: test
  image: debian:stable
  before_script:
  - apt-get update; apt-get -y install libyaml-libyaml-perl libdistro-info-perl
  script:
  - perl tools/check-repos repos/debian/*yaml
  only:
  - merge_requests

.validate-changed: &validate_changed
  stage: test
  image: debian:stable
  before_script:
  - apt-get update; apt-get -y install libyaml-libyaml-perl gpg gpgv libwww-perl libhash-case-perl git diffstat libdistro-info-perl torsocks iputils-ping curl jq pgpainless-cli
  - set -e
  - set -o pipefail
  - git remote add target $CI_MERGE_REQUEST_PROJECT_URL
  - git remote update target
  - git diff target/master..HEAD
  - git diff target/master..HEAD | diffstat -l
  - |
    if [ ! -z "$VALIDATE_ONIONS" ]
    then
      export TORSOCKS_TOR_ADDRESS="$(ping -4 -c1 tor | head -n1 | sed -e 's/^.*  (\([^)]*\)).*$/\1/')"
      echo $TORSOCKS_TOR_ADDRESS
      . torsocks on
      while [ "$(curl https://check.torproject.org/api/ip|jq .IsTor)" != "true" ]
      do
        sleep 1
      done
    fi
  script:
  - tools/validate-repo $(git diff $(diff --old-line-format='' --new-line-format='' <(git rev-list --first-parent "target/master") <(git rev-list --first-parent "HEAD") | head -1)..HEAD | diffstat -l -p 1 | grep ^repos/debian)
  only:
  - merge_requests

validate-changed:
  <<: *validate_changed

validate-changed-onion:
  services:
  - name: registry.salsa.debian.org/extrepo-team/extrepo-data/tor:latest
    alias: tor
  variables:
    VALIDATE_ONIONS: 1
  <<: *validate_changed

.validate-all: &validate_all
  image: debian:stable
  before_script:
  - apt-get update; apt-get -y install libyaml-libyaml-perl gpg gpgv libwww-perl libhash-case-perl libdistro-info-perl torsocks iputils-ping curl jq pgpainless-cli
  - |
    if [ ! -z "$VALIDATE_ONIONS" ]
    then
      export TORSOCKS_TOR_ADDRESS="$(ping -4 -c1 tor | head -n1 | sed -e 's/^.*  (\([^)]*\)).*$/\1/')"
      echo $TORSOCKS_TOR_ADDRESS
      . torsocks on
      while [ "$(curl https://check.torproject.org/api/ip|jq .IsTor)" != "true" ]
      do
        sleep 1
      done
    fi
  script:
  - tools/validate-repo repos/debian/*yaml
  only:
  - schedules

validate-all:
  <<: *validate_all

validate-all-onion:
  services:
  - name: $CI_REGISTRY_IMAGE/tor
    alias: tor
  variables:
    VALIDATE_ONIONS: 1
  <<: *validate_all
